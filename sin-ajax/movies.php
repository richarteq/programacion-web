<?php
date_default_timezone_set('America/Lima');
echo date('l jS \of F Y h:i:s A');
echo '<br>';

$mysqli = new mysqli("localhost", "iw", "12345678", "iw");

/* Comprueba la conexión */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

/* Selecciona todas las filas */
$consulta = "SELECT * FROM movies";

if ($resultado = $mysqli->query($consulta)) {

    /* obtener el array de objetos */
    while ($obj = $resultado->fetch_object()) {
        printf ("%s (%s)<button>Eliminar</button><br>", $obj->id, $obj->title);
    }

    /* liberar el conjunto de resultados */
    $resultado->close();
}

$resultado->close();

/* Cierra la conexión */
$mysqli->close();
?>
